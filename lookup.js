$(function(){

  //either get location or focus on address input.
  if($('#address_from_url').val())
    getLocation();
  /*else
    $('input.address').focus();*/

  $('a.get-coordinates').on('click', function(){
    $('.get-address-container').fadeOut(1, function(){
      $('.get-coordinates-container').fadeIn(1);
      $('input.c_lat').focus();
    });
  });

  $('a.back-to-address-link').on('click', function(){
    $('.get-coordinates-container').fadeOut(1, function(){
      $('.get-address-container').fadeIn(1);
      $('input.address').focus();
    });
  });

  function getLocation(){

    var address = $('input.address').val();

    $.post('get_location.php', {q: address}, function(data){

      //console.log(google_geolocation_url);

      var url = '/?addr='+encodeURIComponent(address).replace('%20','+');
      title = address+' - LPFM Channel Finder - LPFM NOW!'; //!!!doesn't work

      if (history.pushState) {
        window.history.pushState('', title, url);
      }


      $('div.channels div.data').html(' ');
      $('.second-adjacents').fadeIn();
      $('.available .help').show();
      $('.second-adjacents .help').show();


      createSpinner('available_data1');
      $('#available_data1').addClass('av_loading');


      if(typeof data.results[0] == 'undefined'){
        alert('Address not found.');
      }
      else{

        var lat = data.results[0].geometry.location.lat;
        var lng = data.results[0].geometry.location.lng;

        $('.help .lat').html(lat);
        $('.help .lng').html(lng);

        var lat2 = decimalToTime(lat);
        var lng2 = decimalToTime(lng);

        var coordinates = 'LAT: '+lat2.d+'&deg;&nbsp;'+lat2.m+'\'&nbsp;'+lat2.s+'"<br>LNG: '+lng2.d+'&deg;&nbsp;'+lng2.m+'\'&nbsp;'+lng2.s+'"<br>';

        $('div.coordinates div.data').html(coordinates);

        getFCCdata(lat, lng);
        //getNAD27(lat, lng);

        $('.info').fadeIn();

        mapLocation(lat, lng);

      }

    }, 'json');

  }

  function parseFCCdata(data){

    var x;
    var out = new Object;
    out.array = new Array;
    out.error = '';

    x = $(data).find('pre:first').html();

    //handle special cases, ahhhh.
    if(
        typeof x == 'undefined' ||
        x.indexOf("shown are blocked by the station") !== -1
      ){
      out.error = '<span class="none">No channels available at this location.</span>';
    }
    else {
      x = x.replace(/\n/g,"|");
      //console.log(x);
      //x = x.replace(',',"");
      out.array = x.split('|');
    }

    return out;

  }



  function getFCCdata(x, y) {

    var lat = decimalToTime(x);
    var lng = decimalToTime(y);

    var fcc_query = 'dlat='+lat.d+'&mlat='+lat.m+'&slat='+lat.s+'&dlon='+lng.d+'&mlon='+lng.m+'&slon='+lng.s+'&detail=0&xif=0';

    //add &x2nd=1 for link
    var fcc_query_for_link = fcc_query + '&x2nd=1';


    $('.fcc_link').attr('href', 'http://transition.fcc.gov/fcc-bin/lpfm_channel_finder?'+fcc_query_for_link);
    $('.mylpfm_link').attr('href', 'http://mylpfm.com/search/?lat_deg='+lat.d+'&lat_min='+lat.m+'&lat_sec='+lat.s+'&lat_dir=N&lat_dec=&lon_deg='+lng.d+'&lon_min='+lng.m+'&lon_sec='+lng.s+'&lon_dir=W&lon_dec=&mode=geochk&notch=&submode=widearea');

    var d1, d2, d_all, d_2nd;
    var available = '';
    var available2 = '';

    $.post('/get_fcc.php', { q: fcc_query, x2nd: 0 }, function(data1){

      d1 = parseFCCdata(data1);
      d1_a = d1.array;

      var add1 = function(value, key, list){
        if(value != 'undefined')
          available += '<div class="channel-open">'+value+'</div>';
        //else alert(list);
      }

      _.each(d1_a, add1);

      // alert(1);


      $('#available_data1').removeClass('av_loading');

      if(available){
        $('div.available div.data').html(available);
      }
      else{
        $('div.available div.data').html('');
        // $('div.available div.data').html('None.');
        $('.available .help').hide();
      }

      /*
      */

      //start loading appearance for part 2
      createSpinner('available_data2');
      $('#available_data2').addClass('av_loading');


      $.post('/get_fcc.php', { q: fcc_query, x2nd: 1 }, function(data2){


        d2 = parseFCCdata(data2);

        if(!d2.error) {

          d2_a = d2.array;

          //get only-2nd-adjacent channels
          d_2nd = _.difference(d2_a, d1_a);

          var add_2nd = function(value, key, list){
            if(value != 'undefined'){
              available2 += '<div class="channel-second-adjacent">'+value+'</div>';
              //alert(1);
            }
            //else alert(list);
          }
          _.each(d_2nd, add_2nd);

          //else no 2nd-adjacent channels
          if(available2){
            $('div.second-adjacents div.data').html(available2);
            $('#available_data2').removeClass('av_loading');
          }else{
            $('div.second-adjacents div.data').html('None.');
            $('.second-adjacents .help').hide();
              $('#available_data2').removeClass('av_loading');
            /*
            $('.second-adjacents').slideUp(function(){
            });
            */
          }

        }else{ //else none found

          $('div.available div.data').html(d2.error);
          $('.second-adjacents').fadeOut();

        }

      });



    });

  }

/*
  function getNAD27(x, y) {

    var lat = decimalToTime(x);
    var lng = decimalToTime(y);

    var available = '';

    var query = {lat: x, lng: y};


    $.post('/get_nad27.php', query, function(data){

      //alert(data);

    });

  }
*/

  function precise_round(value, decPlaces){
      var val = value * Math.pow(10, decPlaces);
      var fraction = (Math.round((val-parseInt(val))*10)/10);

      //this line is for consistency with .NET Decimal.Round behavior
      // -342.055 => -342.06
      if(fraction == -0.5) fraction = -0.6;

      val = Math.round(parseInt(val) + fraction) / Math.pow(10, decPlaces);
      return val;
  }


  function decimalToTime(dec, lat_lng){

    var out = new Object;

    /*
    out.direction = '';

    //convert negative to positive
    if(dec<0 && lat_lng == 'lng'){
      dec = Math.abs(dec);
      if(lat_lng == 'lng')
        out.direction = 'W';
    }
    if(){
      dec = Math.abs(dec);
    }
    */

    if(dec<0){
      dec = Math.abs(dec);
    }

    out.d = Math.floor(dec);
    var m1 = (dec % 1) * 60;
    out.m = Math.floor(m1);
    out.s = precise_round((m1 % 1) * 60, 2);
    //alert(out.m);

    return out;
  }


  $('#get-address-form').on('submit', function(e){
    //If changing the browser URL through JS isn't supported (IE8),
    //ust submit the form instead, so that new page is loaded with new URL
    if(history.pushState){
      getLocation();
      e.preventDefault();
    }
  });


  //start out with wash dc
  var map = L.map('map', {center: [38.8900, -77.0300], zoom: 12, scrollWheelZoom: false});
  // add an OpenStreetMap tile layer
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  //add a container for the markers
  /*var markers;
  map.addLayer(markers);
    map.removeLayer(markers);
  */

  var already_mapped_once = false;

  var marker, circle;

  function mapLocation(x, y){

    if(already_mapped_once){
      map.removeLayer(circle);
      map.removeLayer(marker);
    }

    map.setView([x, y], 12);

    // add an OpenStreetMap tile layer
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    marker = L.marker([x, y]).addTo(map);

    circle = L.circle([x, y], 5600, {
      color: 'white',
      fillColor: '#fff',
      fillOpacity: 0.3
    }).addTo(map);

    already_mapped_once = true;

    //L.bindPopup("Radius: <br> 5.6km or 3.5mi").openPopup();

    //var popup = L.popup();
  }



  function createSpinner(id){

    var opts = {
      lines: 8, // The number of lines to draw
      length: 3, // The length of each line
      width: 4, // The line thickness
      radius: 10, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: '0px' // Left position relative to parent in px
    };
    var target = document.getElementById(id);
    var spinner = new Spinner(opts).spin(target);

  }


});