<?

//$address = 90210;
$address = '';

$lat = $_GET['lat'];
$lng = $_GET['lng'];
$addr = $_GET['addr'];

?><!DOCTYPE html>
<html>
<head>
  <title>Low Power Radio Channel Finder - LPFM NOW!</title>

  <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.css" />
  <!--[if lte IE 8]>
      <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.ie.css" />
  <![endif]-->
  <script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js"></script>
  <script src="underscore-1-4-4-min.js"></script>
  <script src="spin.min.js"></script>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
  <link href='http://fonts.googleapis.com/css?family=Antic+Slab|Stalinist+One|Londrina+Shadow|Quantico|Expletus+Sans|Prosto+One|Cutive|Signika' rel='stylesheet' type='text/css'>

  <link href="bootstrap/css/bootstrap.css" rel="stylesheet" />

  <link href="css.css" rel="stylesheet" />

</head>


<body>

<!-- http://maps.google.com/maps/api/geocode/json?address=4228+Olive+Hwy+Oroville+CA&sensor=false -->

<input type="hidden" value="<?=$addr?>" id="address_from_url" />

<div class="container">

  <div class="row-fluid">
    <div class="span5 content">

      <div class="top-powered-by">
        <p>Powered by <a href="http://commonfrequency.org">Common Frequency</a></p>
      </div>

      <div class="section heading">

        <div class="inner">

          <div class="logo">
            <a href="http://lpfmnow.org"><img src="logo1.png" /></a>
          </div>


          <div class="inputs">

            <div class="get-address-container">

              <div class="intro">
                <p>Visualize a new <b>Low Power FM</b> radio station, and find available radio channels in your area.</p>
                <!--<p>Enter a <b>city/state</b>, <b>address</b>, or <b>zip&nbsp;code</b>.</p>-->
              </div>

              <form id="get-address-form" action="/" method="get">
                <input type="text" class="address" name="addr" value="<?=$addr?>" placeholder="ex. Denver, CO or 80210"/>
                <div class="submit-container">
                  <button class="get-address" type="submit">Check Location</button>
                </div>
                <!--  -->
              </form>

              <!-- <div class="coords-instead ">
                <p class="grey">(To use <b>coordinates</b>, <a href="javascript:" class="get-coordinates">click here</a>.)</p>
              </div> -->

            </div>

            <div class="get-coordinates-container" style="display:none;">

              <form id="get-coordinates-form" class="get-coordinates">

                <div class="row-fluid">
                  <div class="span2 label">LAT</div>
                  <input type="text" class="span10 c_lat" placeholder="D M S"/>
                </div>
                <div class="row-fluid">
                  <div class="span2 label">LNG</div>
                  <input type="text" class="span10 c_lng" placeholder="D M S"/>
                </div>
                <div class="row-fluid">
                  <div class="span2">
                  </div>
                  <div class="span10">
                    <div class="help">Ex. <span class="code">39 26 15</span> or <span class="code">122 26 15.75</span></div>
                  </div>
                </div>
                <div class="row-fluid">
                  <div class="span2">
                  </div>
                  <div class="span10">
                    <div class="submit-container">
                      <button type="submit" class="btn btn-success btn-large">Seach</button>
                    </div>
                  </div>
                </div>

                <div class="grey back-to-address">&lt; <a href="javascript:" class="back-to-address-link">Back to address form </a></div>

                <!--  -->
              </form>

            </div> <!-- close coordinates -->

          </div>

        </div>

      </div>

      <div class="share" style="display:none;">
        Share this search: <a href="#">Facebook</a>
      </div>

      <div class="section info" style="display:none;">

        <div class="inner">

          <div class="location-info">

            <div class="location-data">
              <div class="box coordinates">
                <div class="data"></div>
                <div class="help">DEC:&nbsp;<span class="lat"></span>,&nbsp;<span class="lng"></span></div>
                <div class="help">Coordinates are in NAD83</div>
              </div>
              <div class="box antenna">
                <b>LP-100 - 60dBu Contour</b><br>
                5.6km (3.5mi) radius<br>
                <!--<div class="help">LP-100 dBu</div>-->
              </div>
            </div>

            <div class="channels-container">

              <div class="box channels available">
                <h3>Available Channels</h3>
                <div class="data" id="available_data1"></div>
                <div class="help">These channels are most likely available.</div>
              </div>
              <div id="second-adjacents-loading"></div>
              <div class="box channels second-adjacents">
                <h3>Second-Adjacent Channels</h3>
                <div class="data" id="available_data2"></div>
                <div class="help">These channels may be available if <a href="http://www.fcc.gov/encyclopedia/low-power-fm-lpfm-channel-finder" target="_blank">certain conditions</a> are met.</div>
              </div>

            </div>

            <div class="tools">
              <h3>Confirm this location on FCC.gov</h3>
              <ul>
              <li><a href="#" class="fcc_link" target="_blank">FCC.gov LPFM Query</a></li>
              </ul>
              <h3>More Channel &amp; Tower Options</h3>
              <ul>
              <li><a href="#" class="mylpfm_link" target="_blank">myLPFM Search from REC</a></li>
              </ul>
            </div>
            <div class="box consult">
              Ready to start an LPFM station?<br>
              <a href="http://commonfrequency.org/apply">Consult with Common Frequency</a>
            </div>
          </div>

        </div>

      </div>

      <div class="row-fluid">
        <div class="span12">
          <div class="footer">
            <p>Powered by <a href="http://commonfrequency.org">Common Frequency</a></p>
          </div>
        </div>
      </div>
<!--
-->

    </div>

    <div class="section span7">
      <div class="map_container">
        <div id="map"></div>
        <div id="map_border"></div>
      </div>
    </div>

  </div>

</div>

<script src="lookup.js"></script>

<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=9043400;
var sc_invisible=1;
var sc_security="dfc2847e";
var sc_https=1;
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="web stats"
href="http://statcounter.com/" target="_blank"><img
class="statcounter"
src="http://c.statcounter.com/9043400/0/dfc2847e/1/"
alt="web stats"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->


</body>
</html>